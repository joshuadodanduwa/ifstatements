/*
if Practice

Title:  New PIN Number

put relevant comments here and for your program code

*/

let pinNum1 = 0
let pinNum2 = 0


pinNum1 = Number(prompt("PIN setup - Stage 1.\nEnter your 4 digit number (1000 to 9999):"))

if (pinNum1 >= 1000 && pinNum1 <= 9999) { 
    pinNum2 = Number(prompt("PIN setup - Stage 2.\nRe-enter your 4 digit number (1000 to 9999):"))
    if (pinNum1 === pinNum2){
        console.log("Your PIN has been set.")
    }
    else {
        console.log("Error! Your PIN numbers do not match.  Your PIN was not set.")
    }

} else {
    console.log("This PIN number is not within the the right parameters.")
} 

//The following line of code may be of use if you have problems
console.log(`First Number: ${pinNum1}  Second Number: ${pinNum2}`)